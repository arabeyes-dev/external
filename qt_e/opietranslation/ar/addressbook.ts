<!DOCTYPE TS>
<TS>
  <context>
    <name>AbTable</name>
    <message>
      <source>Full Name</source>
      <translation>الاسم الكامل</translation>
    </message>
    <message>
      <source>Contact</source>
      <translation>متّصل به</translation>
    </message>
    <message>
      <source>Pick</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>AddressbookWindow</name>
    <message>
      <source>Contacts</source>
      <translation>متصل بهم</translation>
    </message>
    <message>
      <source>Contact</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>New</source>
      <translation>جديد</translation>
    </message>
    <message>
      <source>Edit</source>
      <translation>حور</translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>مسح</translation>
    </message>
    <message>
      <source>Find</source>
      <translation>إيحاد</translation>
    </message>
    <message>
      <source>Write Mail To</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Beam Entry</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>My Personal Details</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>View</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Can not edit data, currently syncing</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Edit My Personal Details</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Edit Address</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Contacts - My Personal Details</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Out of space</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Unable to save information.
Free up some space
and try again.

Quit anyway?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>All</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Unfiled</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Import vCard</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Close Find</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Save all Data</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Config</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Not Found</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>List</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cards</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Card</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Start Search</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Right file type ?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>&amp;Yes</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>&amp;No</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Unable to find a contact for this search pattern!</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>The selected file 
 does not end with &quot;.vcf&quot;.
 Do you really want to open it?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Add Contact?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Do you really want add contact for 
%1?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>&amp;All Yes</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Export vCard</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>You have to select a contact !</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>You have to set a filename !</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>ConfigDlg_Base</name>
    <message>
      <source>Query Style</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Use Regular Expressions</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Use Wildcards (*,?)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Case Sensitive</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mail</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Prefer QT-Mail </source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Prefer Opie-Mail</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Notice: QT-Mail is just 
provided in the SHARP 
default ROM. Opie-Mail 
is provided free !</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Misc</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Search Settings</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Font</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Small</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Normal</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Large</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Order</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Select Contact Order:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Up</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Down</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Add</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Remove</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Configuration</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Click on tab to select one</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Settings for the search query style</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Search widget expects regular expressions if selected</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Search widget just expects simple wildcards</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>If selected, search differs between upper and lower chars</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Font size for list- and card view</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Fontsettings for list and card view</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Use Sharp's mail application if available</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Use OPIE mail if installed</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Move selected attribute one line up</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Move selected attribute one line down</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>List of all available attributes</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Add selected attribute from list below to the upper list</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Remove the selected attribute from the upper list</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Order (up -> down) defines the primary contact shown in the second column of the list view</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tool-/Menubar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Fixed</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Switch to fixed menu-/toolbars after restarting application !</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Moveable</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Switch to moveable menu-/toolbars after restarting application !</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>ContactEditor</name>
    <message>
      <source>First Name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Middle Name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Last Name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Suffix</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>File As</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Gender</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Job Title</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>City</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>State</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Country</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Full Name...</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Organization</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Category</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Notes...</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>General</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Business</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Home</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Address</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Zip Code</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>United States</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>United Kingdom</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Japan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>France</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Germany</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Norway</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Canada</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Male</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Female</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Details</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Enter Note</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Edit Name</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Contacts</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Albania</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Algeria</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>American Samoa</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Andorra</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Angola</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Anguilla</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Argentina</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Aruba</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Australia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Austria</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Azerbaijan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bahamas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bahrain</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bangladesh</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Barbados</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Belarus</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Belgium</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Belize</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Benin</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bermuda</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bhutan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Botswana</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bouvet Island</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Brazil</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Brunei Darussalam</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bulgaria</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Burkina Faso</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Burundi</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cambodia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cape Verde</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cayman Islands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Chad</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Chile</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>China</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Christmas Island</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Colombia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Comoros</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Congo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cook Island</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Costa Rica</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cote d'Ivoire</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Croatia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cuba</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cyprus</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Czech Republic</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Denmark</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Djibouti</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Dominica</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Dominican Republic</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>East Timor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Ecuador</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Egypt</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>El Salvador</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Equatorial Guinea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Eritrea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Estonia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Ethiopia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Falkland Islands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Faroe Islands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Fiji</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Finland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>French Guiana</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>French Polynesia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Gabon</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Gambia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Georgia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Gibraltar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Greece</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Greenland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Grenada</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Guam</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Guatemala</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Guinea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Guyana</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Haiti</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Holy See</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Honduras</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Hong Kong</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Hungary</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Iceland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>India</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Indonesia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Ireland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Israel</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Italy</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Jordan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Kazakhstan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Kenya</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Korea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Laos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Latvia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Lebanon</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Lesotho</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Liberia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Liechtenstein</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Lithuania</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Luxembourg</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Macau</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Macedonia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Madagascar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Malawi</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Malaysia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Maldives</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mali</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Malta</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Martinique</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mauritania</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mauritius</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mayotte</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mexico</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Micronesia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Moldova</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Monaco</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mongolia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Montserrat</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Morocco</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Mozambique</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Myanmar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Namibia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Nauru</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Nepal</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Netherlands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>New Caledonia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>New Zealand</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Nicaragua</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Niger</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Nigeria</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Niue</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Oman</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Pakistan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Palau</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Panama</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Papua New Guinea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Paraguay</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Peru</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Philippines</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Poland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Portugal</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Puerto Rico</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Qatar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Reunion</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Romania</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Russia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Rwanda</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Saint Lucia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Samoa</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>San Marino</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Saudi Arabia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Senegal</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Seychelles</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Sierra Leone</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Singapore</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Slovakia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Slovenia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Solomon Islands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Somalia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>South Africa</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Spain</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Sri Lanka</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>St. Helena</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Sudan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Suriname</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Swaziland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Sweden</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Switzerland</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Taiwan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tajikistan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tanzania</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Thailand</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Togo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tokelau</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tonga</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tunisia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Turkey</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Turkmenistan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Tuvalu</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Uganda</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Ukraine</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Uruguay</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Uzbekistan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Vanuatu</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Venezuela</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Virgin Islands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Western Sahara</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Yemen</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Yugoslavia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Zambia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Zimbabwe</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Birthday</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Anniversary</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Kuwait</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Unknown</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Delete</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Afghanistan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Antarctica</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Armenia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Bolivia</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Cameroon</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Ghana</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Guadeloupe</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Guinea-Bissau</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Jamaica</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Kiribati</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Kyrgyzstan</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Palestinian Sovereign Areas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Pitcairn Islands</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Vietnam</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Press to enter last- middle and firstname</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Enter fullname directly ! If you have a lastname with multiple words ( for instance &quot;de la Guerra&quot;), please write &lt;lastname>,&lt;firstnames> like this: &quot;de la Guerra, Carlos Pedro&quot;</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>The jobtitle..</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Something like &quot;jr.&quot;..</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>The working place of the contact</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Press to select attribute to change</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source>Press to select how to store the name (and howto show it in the listview)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <source></source>
      <translation type="unfinished" />
    </message>
  </context>
</TS>
